package coE;

public class Einheit {

	private String name;
	private int kosten;
	private String bild;

	public Einheit(String name, int kosten, String bild) {
		super();
		this.name = name;
		this.kosten = kosten;
		this.bild = bild;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKosten() {
		return kosten;
	}

	public void setKosten(int Kosten) {
		this.kosten = Kosten;
	}

	public String getBild() {
		return bild;
	}

	public void setBild(String bild) {
		this.bild = bild;
	}

	@Override
	public String toString() {
		return "Einheit [name=" + name + ", Kosten=" + kosten + ", bild=" + bild + "]";
	}

}
