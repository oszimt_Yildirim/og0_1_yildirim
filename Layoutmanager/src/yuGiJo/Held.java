package yuGiJo;

public class Held {

	private String name;
	private String typ;
	private String beschreibung;
	private int maxHealth;
	private int currentHealth;
	private int damage;
	private int armor;
	private int mResist;
	private String bild;

	public Held(String name, String typ, String beschreibung, int maxHealth, int currentHealth, int damage, int armor,
			int mResist, String bild) {
		super();
		this.name = name;
		this.typ = typ;
		this.beschreibung = beschreibung;
		this.maxHealth = maxHealth;
		this.currentHealth = currentHealth;
		this.damage = damage;
		this.armor = armor;
		this.mResist = mResist;
		this.bild = bild;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}

	public int getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = currentHealth;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	public int getmResist() {
		return mResist;
	}

	public void setmResist(int mResist) {
		this.mResist = mResist;
	}

	public String getBild() {
		return bild;
	}

	public void setBild(String bild) {
		this.bild = bild;
	}

	
	@Override
	public String toString() {
		return "Held [name=" + name + ", typ=" + typ + ", beschreibung=" + beschreibung + ", maxHealth=" + maxHealth
				+ ", currentHealth=" + currentHealth + ", damage=" + damage + ", armor=" + armor + ", mResist="
				+ mResist + ", bild=" + bild + "]";
	}
}
