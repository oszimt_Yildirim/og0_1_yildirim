package yuGiJo;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JTextPane;
import java.awt.Font;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

public class KartenGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					KartenGUI frame = new KartenGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public KartenGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 550);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(184, 134, 11));
		contentPane.setForeground(new Color(184, 134, 11));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel Kopf = new JPanel();
		Kopf.setBorder(new BevelBorder(BevelBorder.RAISED, new Color(255, 0, 0), new Color(255, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)));
		Kopf.setBackground(new Color(165, 42, 42));
		Kopf.setForeground(new Color(165, 42, 42));
		contentPane.add(Kopf, BorderLayout.NORTH);
		Kopf.setLayout(new GridLayout(1, 0, 0, 0));

		JLabel lblNewLabel = new JLabel("Heldname");
		lblNewLabel.setFont(new Font("Arial", Font.BOLD, 18));
		lblNewLabel.setForeground(new Color(255, 255, 0));
		Kopf.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Typ");
		lblNewLabel_1.setFont(new Font("Arial", Font.BOLD, 18));
		lblNewLabel_1.setForeground(new Color(255, 255, 0));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		Kopf.add(lblNewLabel_1);

		JPanel Beschreibung = new JPanel();
		Beschreibung.setBorder(new BevelBorder(BevelBorder.RAISED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(255, 0, 0), new Color(255, 0, 0)));
		Beschreibung.setBackground(new Color(165, 42, 42));
		Beschreibung.setForeground(new Color(165, 42, 42));
		contentPane.add(Beschreibung, BorderLayout.SOUTH);
		Beschreibung.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setBackground(new Color(165, 42, 42));
		Beschreibung.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(4, 0, 0, 0));

		JLabel lblNewLabel_2 = new JLabel(" Leben:");
		lblNewLabel_2.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_2.setForeground(new Color(255, 255, 0));
		panel.add(lblNewLabel_2);

		JLabel lblNewLabel_4 = new JLabel(" Ruestung:");
		lblNewLabel_4.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_4.setForeground(new Color(255, 255, 0));
		panel.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel(" Magieresistenz:");
		lblNewLabel_5.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_5.setForeground(new Color(255, 255, 0));
		panel.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel(" Angriff:");
		lblNewLabel_6.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_6.setForeground(new Color(255, 255, 0));
		panel.add(lblNewLabel_6);

		JTextPane txtpnGg = new JTextPane();
		txtpnGg.setFont(new Font("Tahoma", Font.ITALIC, 12));
		txtpnGg.setText(" Text...\r\n \r\n \r\n \r\n ");
		txtpnGg.setForeground(new Color(255, 215, 0));
		txtpnGg.setBackground(new Color(165, 42, 42));
		Beschreibung.add(txtpnGg, BorderLayout.NORTH);

		JTextPane textPane = new JTextPane();
		textPane.setText("\r\n\r\n");
		textPane.setBackground(new Color(165, 42, 42));
		Beschreibung.add(textPane, BorderLayout.SOUTH);

		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setIcon(new ImageIcon("./src/Kraehe.png"));
		contentPane.add(lblNewLabel_7, BorderLayout.CENTER);
	}

}
