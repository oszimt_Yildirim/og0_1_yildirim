package erweiterung;

public class Addon {

	private int idNummer;
	private String bezeichnung;
	private double verkaufspreis;
	private int menge;
	private int maxAnzahl;
	
	public Addon()
	{
	}
	
	public int getIdNummer() {
		return idNummer;
	}
	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}
	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public double getVerkaufspreis() {
		return verkaufspreis;
	}
	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}
	public int getMaxAnzahl() {
		return maxAnzahl;
	}
	public void setMaxAnzahl(int maxAnzahl) {
		this.maxAnzahl = maxAnzahl;
	}
	
}
