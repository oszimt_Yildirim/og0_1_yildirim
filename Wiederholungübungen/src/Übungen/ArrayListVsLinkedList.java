package �bungen;

import java.util.ArrayList;
import java.util.List;

public class ArrayListVsLinkedList {

	public static void main(String[] args) {

		List<Integer> zahlenliste = new ArrayList<Integer>();

		zahlenliste.add(14);
		zahlenliste.add(22);
		zahlenliste.add(24);
		zahlenliste.add(30);
		zahlenliste.add(11);
		zahlenliste.add(18);

		System.out.println(zahlenliste);

		zahlenliste.size();
		
		System.out.println(zahlenliste);
		
		zahlenliste.remove(14);
		
		System.out.println(zahlenliste);

		zahlenliste.clear();
		
		System.out.println(zahlenliste);
	}

}
