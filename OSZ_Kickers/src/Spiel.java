
public class Spiel {

	private char heimOderGast;
	private String datum;
	private String ergebnis;

	public Spiel() {}
	public Spiel(char heimOderGast, String datum, String ergebnis) {
		
		this.heimOderGast = heimOderGast;
		this.datum = datum;
		this.ergebnis = ergebnis;
	}
	public char getHeimOderGast() {
		return heimOderGast;
	}
	public void setHeimOderGast(char heimOderGast) {
		this.heimOderGast = heimOderGast;
	}
	public String getDatum() {
		return datum;
	}
	public void setDatum(String datum) {
		this.datum = datum;
	}
	public String getErgebnis() {
		return ergebnis;
	}
	public void setErgebnis(String ergebnis) {
		this.ergebnis = ergebnis;
	}
	
	
}
