
public class Mannschaftsleiter extends Mitglieder {

	
	private String mannschaftsname;
	private int rabatt;
	
	public Mannschaftsleiter() {}
	public Mannschaftsleiter(String mannschaftsname, int rabatt) {
		
	super(vorname, nachname, telefonnummer, jahresbeitrag);
	
	this.mannschaftsname = mannschaftsname;
	this.rabatt= rabatt;
	}
	
	public String getMannschaftsname() {
		return mannschaftsname;
	}
	public void setMannschaftsname(String mannschaftsname) {
		this.mannschaftsname = mannschaftsname;
	}
	public int getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}

}
