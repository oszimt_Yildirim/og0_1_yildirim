
public class TestKickers {

public static void main(String[] args) {
	
	Spieler s1 = new Spieler();
	s1.setVorname("Donald");
	s1.setNachname("Duck");
	s1.setTelefonnummer("+99 456 789");
	s1.setJahresbeitrag(false);
	s1.setSpielposition("St�rmer");
	s1.setTrikotnummer("42");
	
	System.out.println("Spieler nr.1:");
	System.out.println(s1.getVorname());
	System.out.println(s1.getNachname());
	System.out.println(s1.getTelefonnummer());
	System.out.println(s1.isJahresbeitrag());
	System.out.println(s1.getSpielposition());
	System.out.println(s1.getTrikotnummer());
	
	System.out.println(" ");
	
	Spieler s2 = new Spieler();
	s2.setVorname("Daniel");
	s2.setNachname("D�sentrieb");
	s2.setTelefonnummer("+99 478 684");
	s2.setJahresbeitrag(true);
	s2.setSpielposition("Torwart");
	s2.setTrikotnummer("11010");
	
	System.out.println("Spieler nr.2:");
	System.out.println(s2.getVorname());
	System.out.println(s2.getNachname());
	System.out.println(s2.getTelefonnummer());
	System.out.println(s2.isJahresbeitrag());
	System.out.println(s2.getSpielposition());
	System.out.println(s2.getTrikotnummer());
	
	System.out.println(" ");
	
	Trainer t1 = new Trainer();
	t1.setVorname("Mickey");
	t1.setNachname("Maus");
	t1.setTelefonnummer("+99 123 456");
	t1.setJahresbeitrag(true);
	t1.setLizenzklasse('C');
	t1.setAufwandentschaedigung(85.00);
	
	System.out.println("Trainer:");
	System.out.println(t1.getVorname());
	System.out.println(t1.getNachname());
	System.out.println(t1.getTelefonnummer());
	System.out.println(t1.isJahresbeitrag());
	System.out.println(t1.getLizenzklasse());
	System.out.println(t1.getAufwandentschaedigung());
	
	System.out.println(" ");
	
	Mannschaft m1 = new Mannschaft();
	m1.setName("Mickey's Wunderteam");
	m1.setSpielklasse('B');
	
	System.out.println("Mannschaft:");
	System.out.println(m1.getName());
	System.out.println(m1.getSpielklasse());
	
	System.out.println(" ");
	
	Spiel sp1 = new Spiel();
	sp1.setDatum("10.04.2021");
	sp1.setHeimOderGast('h');
	sp1.setErgebnis("1 - 0");
	
	System.out.println("Spiel nr.1:");
	System.out.println(sp1.getDatum());
	System.out.println(sp1.getHeimOderGast());
	System.out.println(sp1.getErgebnis());
	
	System.out.println(" ");
	
	Spiel sp2 = new Spiel();
	sp2.setDatum("29.05.2021");
	sp2.setHeimOderGast('g');
	sp2.setErgebnis("2 - 2");
	
	System.out.println("Spiel nr.2:");
	System.out.println(sp2.getDatum());
	System.out.println(sp2.getHeimOderGast());
	System.out.println(sp2.getErgebnis());

	
}	
			
}
