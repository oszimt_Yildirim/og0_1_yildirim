
public class Trainer extends Mitglieder {

	
	private char lizenzklasse;
	private double aufwandentschaedigung;
	
	public Trainer() {}
	public Trainer(char lizenzklasse, double aufwandentschaedigung) {
		
		super(vorname, nachname, telefonnummer, jahresbeitrag);
		
		this.lizenzklasse = lizenzklasse;
		this.aufwandentschaedigung = aufwandentschaedigung;
	}
	public char getLizenzklasse() {
		return lizenzklasse;
	}
	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}
	public double getAufwandentschaedigung() {
		return aufwandentschaedigung;
	}
	public void setAufwandentschaedigung(double aufwandentschaedigung) {
		this.aufwandentschaedigung = aufwandentschaedigung;
	}
	
}
