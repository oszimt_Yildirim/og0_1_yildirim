
public class Spieler extends Mitglieder{

	private String trikotnummer;
	private String spielposition;
	
	public Spieler() {}
	public Spieler(String trikotnummer, String spielposition) {
		
		super(vorname, nachname, telefonnummer, jahresbeitrag);
		
		this.trikotnummer = trikotnummer;
		this.spielposition = spielposition;
	}
	
	public String getTrikotnummer() {
		return trikotnummer;
	}
	public void setTrikotnummer(String trikotnummer) {
		this.trikotnummer = trikotnummer;
	}
	public String getSpielposition() {
		return spielposition;
	}
	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}
	
}
