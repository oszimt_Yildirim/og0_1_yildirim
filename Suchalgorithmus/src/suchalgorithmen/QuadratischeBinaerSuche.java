package suchalgorithmen;

import listengenerator.SortedListGenerator;

public class QuadratischeBinaerSuche implements ISuchalgorithmus {

  @Override
  public int suche(long[] zahlen, long gesuchteZahl) {
    int l = 0, r = zahlen.length - 1;
    int m;
    int sprung;

    while (l <= r) {
      // Bereich interpolieren
      m = (l + (int) ((double) (gesuchteZahl - zahlen[l]) / (zahlen[r] - zahlen[l]) * (r - l)));
      // Intervall bestimmen
      sprung = (int) Math.round(Math.sqrt(r - l));

      if (zahlen[m] == gesuchteZahl) // Element gefunden?
        return m;

      if (zahlen[m] > gesuchteZahl) {
        r = m - 1; // in linker Seite suchen
        while (l <= r - sprung) {
          if (zahlen[r - sprung] == gesuchteZahl)
            return r - sprung;
          if (zahlen[r - sprung] > gesuchteZahl)
            r = r - sprung - 1;
          else
            l = r - sprung + 1;
        }
      } else {
        l = m + 1; // in linker Seite suchen
        while (r >= l + sprung) {
          if (zahlen[l + sprung] == gesuchteZahl)
            return l + sprung;
          if (zahlen[l + sprung] > gesuchteZahl)
            r = l + sprung - 1;
          else
            l = l + sprung + 1;
        }
      }
    }
    return NICHT_GEFUNDEN;
  }

  @Override
  public int getVersuche(long[] zahlen, long gesuchteZahl) {
    int l = 0, r = zahlen.length - 1;
    int m;
    int sprung;
    int vergleiche = 0;

    while (l <= r) {
      // Bereich interpolieren
      m = (l + (int) ((double) (gesuchteZahl - zahlen[l]) / (zahlen[r] - zahlen[l]) * (r - l)));
      // Intervall bestimmen
      sprung = (int) Math.round(Math.sqrt(r - l));

      vergleiche++;
      if (zahlen[m] == gesuchteZahl) // Element gefunden?
        return vergleiche;

      vergleiche++;
      if (zahlen[m] > gesuchteZahl) {
        r = m - 1; // in linker Seite suchen
        while (l <= r - sprung) {
          vergleiche++;
          if (zahlen[r - sprung] == gesuchteZahl)
            return vergleiche;
          vergleiche++;
          if (zahlen[r - sprung] > gesuchteZahl)
            r = r - sprung - 1;
          else
            l = r - sprung + 1;
        }
      } else {
        l = m + 1; // in linker Seite suchen
        while (r >= l + sprung) {
          vergleiche++;
          if (zahlen[l + sprung] == gesuchteZahl)
            return vergleiche;
          vergleiche++;
          if (zahlen[l + sprung] > gesuchteZahl)
            r = l + sprung - 1;
          else
            l = l + sprung + 1;
        }
      }
    }
    return vergleiche;
  }

  public static void main(String[] args) {
    QuadratischeBinaerSuche s = new QuadratischeBinaerSuche();
    final int ANZAHL = 20000000; // 20.000.000
    long[] zahlen = SortedListGenerator.getSortedList(ANZAHL);
    int gesuchteZahl = ANZAHL / 2;
    System.out.println(s.getVersuche(zahlen, zahlen[gesuchteZahl]));
  }

}