package suchalgorithmen;

import listengenerator.SortedListGenerator;

public class InterpolationsSuche implements ISuchalgorithmus {

  @Override
  public int suche(long[] zahlen, long gesuchteZahl) {
    int l = 0, r = zahlen.length - 1;
    int m;

    while (l <= r) {
      // Bereich interpolieren
      m = (l + (int)((double)(gesuchteZahl - zahlen[l]) / (zahlen[r] - zahlen[l]) * (r - l)));

      if (zahlen[m] == gesuchteZahl) // Element gefunden?
        return m;

      if (zahlen[m] > gesuchteZahl)
        r = m - 1; // im linken Abschnitt weitersuchen
      else
        l = m + 1; // im rechten Abschnitt weitersuchen
    }
    return NICHT_GEFUNDEN;
  }

  @Override
  public int getVersuche(long[] zahlen, long gesuchteZahl) {
    int l = 0, r = zahlen.length - 1;
    int m;
    int vergleiche = 0;

    while (l <= r) {
      // Bereich interpolieren
      m = (l + (int)((double)(gesuchteZahl - zahlen[l]) / (zahlen[r] - zahlen[l]) * (r - l)));

      vergleiche++;
      if (zahlen[m] == gesuchteZahl) // Element gefunden?
        return vergleiche;

      vergleiche++;
      if (zahlen[m] > gesuchteZahl)
        r = m - 1; // im linken Abschnitt weitersuchen
      else
        l = m + 1; // im rechten Abschnitt weitersuchen
    }
    return vergleiche;
  }

  public static void main(String[] args) {
    InterpolationsSuche s = new InterpolationsSuche();
    final int ANZAHL = 20000000; // 20.000.000
    long[] zahlen = SortedListGenerator.getSortedList(ANZAHL);
    long gesuchteZahl = zahlen[ANZAHL / 10];
    System.out.println(s.suche(zahlen, gesuchteZahl));
  }
}